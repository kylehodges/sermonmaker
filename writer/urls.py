from django.urls import path
from .views import current_sermon, login_user

urlpatterns = [
    path('current/', current_sermon, name='current_sermon'),
    path('login/', login_user, name="login_user")
]
