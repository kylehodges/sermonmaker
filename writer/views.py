from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import CurrentSermon
from django.http import JsonResponse
from .common.json import ModelEncoder
import json
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


class CurrentSermonEncoder(ModelEncoder):
    model = CurrentSermon
    properties = [
        "id",
        "sermon"
    ]

@api_view(['GET', 'PUT'])
def current_sermon(request):
    if request.method == 'GET':
        sermon = CurrentSermon.objects.all()
        return JsonResponse(
            {"sermons": sermon},
            encoder=CurrentSermonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sermon = CurrentSermon.objects.get(id=1)
            props = ["sermon"]
            for prop in props:
                if prop in content:
                    setattr(sermon, prop, content[prop])
            sermon.save()
            return JsonResponse(
                {"message": "Great Success!"},
                safe=False
            )
        except CurrentSermon.DoesNotExist:
            return JsonResponse(
                {"message": "no worky"},
                status=400
            )


@api_view(['POST'])
def login_user(request):
    try:
        content = json.loads(request.body)
        username = content["username"]
        password = content["password"]
        user = authenticate(
            request,
            username=username,
            password=password
        )

        if user is not None:
            login(request, user)
            return JsonResponse(
                {"message": "Successful Login"}
            )
    except User.DoesNotExist:
        return JsonResponse(
            {"message": "Incorrect username or password"}
        )
