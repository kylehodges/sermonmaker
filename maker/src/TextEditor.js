import React from "react";
import 'quill/dist/quill.snow.css'
import ReactQuill from 'react-quill'

const TextEditor = () => {

  var modules = {
    toolbar: [
      [{ size: ["small", false, "large", "huge"] }],
      ["bold", "italic", "underline", "blockquote",
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
        { align: [] }
      ],]
  };

  var formats = [
    "header", "height", "bold", "italic",
    "underline", "blockquote",
    "list", "color", "bullet", "indent", "align", "size",
  ];

  const handleProcedureContentChange = async (content) => {
    console.log("content---->", content);
    const url = 'http://localhost:8000/api/current/'
    const data = {};
    data.sermon = content;
    console.log(data.sermon);
    const fetchConfig = {
        method: "PUT",
        body: JSON.stringify(data),
        Headers: {
            'Content-Type': 'application/json',
        }
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok){
        console.log("victory in Jesus");
    }
  };





  return (
    <div >
      <div className="quill" style={{ display: "grid", justifyContent: "center"}}>
        <ReactQuill
          modules={modules}
          formats={formats}
          placeholder="Sermon Notes"
          onChange={handleProcedureContentChange}
          style={{ height: "550px"}}
        >
        </ReactQuill>
      </div>
    </div>
  );

}

export default TextEditor;
