import React, { useEffect, useState } from "react";
import parse from 'html-react-parser';
import { NavLink } from 'react-router-dom';

function Presentation() {
    const [s, setS] = useState('');
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/current/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const v = data.sermons;
            console.log(v[0].sermon);
            setS(v[0].sermon);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
            <div className="pmode-back">
                <button className="home-link"><NavLink className="main-link nl" to="/">Home</NavLink></button>


                <p className="present-panel">{parse(s)}</p>


            </div>
        </div>
    )
}
export default Presentation;
