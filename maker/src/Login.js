import React, { useEffect, useState } from "react";
import { NavLink } from 'react-router-dom';


function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsernameChange = (event) => {
        const v = event.target.value;
        setUsername(v);
    }
    const handlePasswordChange = (event) => {
        const v = event.target.value;
        setPassword(v);
    }

    const handleSubmit = async (event) => {
        event.preventdefalt()
        const url = 'http://localhost:8000/api/login/';
        const data = {};
        data.username = username;
        data.password = password;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            Headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok){
            console.log("Signed in");
        }

    }
    const handleCreate = async (event) => {

    }



    useEffect(() => {

    }, [])


    return (
        <div>
            <button className="home-link"><NavLink className="main-link nl" to="/">Home</NavLink></button>
            <div className="login">
                <form onSubmit={handleSubmit} className="login-form">
                    <h1 className="login-title">Login</h1>
                    <input onChange={handleUsernameChange} className="login-input" type="text" value={username} placeholder="Username"/><br></br>
                    <input onChange={handlePasswordChange} className="login-input" type="password" value={password} placeholder="Password"/><br></br>
                    <button className="login-btn" type="submit">Login</button>
                </form>
            </div>
            <div className="create">
                <form onSubmit={handleCreate} className="create-form">
                    <h1 className="create-title">Sign Up</h1>
                    <input onChange={handleUsernameChange} className="login-input" type="text" value={username} placeholder="Username"/><br></br>
                    <input onChange={handlePasswordChange} className="login-input" type="password" value={password} placeholder="Password"/><br></br>
                    <button className="login-btn" type="submit">Sign Up</button>
                </form>
            </div>
        </div>
    )
}
export default Login;
