import React, { useEffect, useState } from "react";
import { NavLink } from 'react-router-dom';
import TextEditor from './TextEditor.js';

function Write() {
    const [sermon, setSermon] = useState('');
    const handleSermonChange = async (event) => {
        const v = event.target.value;
        console.log("sermon", v);
        setSermon(v);
    }


    const bible = [
        { book: "Genesis", chapters: 50 },
        { book: "Exodus", chapters: 40 },
        { book: "Leviticus", chapters: 27 },
        { book: "Numbers", chapters: 36 },
        { book: "Deuteronomy", chapters: 34 },
        { book: "Joshua", chapters: 24 },
        { book: "Judges", chapters: 21 },
        { book: "Ruth", chapters: 4 },
        { book: "1 Samuel", chapters: 31 },
        { book: "2 Samuel", chapters: 24 },
        { book: "1 Kings", chapters: 22 },
        { book: "2 Kings", chapters: 25 },
        { book: "1 Chronicles", chapters: 29 },
        { book: "2 Chronicles", chapters: 36 },
        { book: "Ezra", chapters: 10 },
        { book: "Nehemiah", chapters: 13 },
        { book: "Esther", chapters: 10 },
        { book: "Job", chapters: 42 },
        { book: "Psalms", chapters: 150 },
        { book: "Proverbs", chapters: 31 },
        { book: "Ecclesiastes", chapters: 12 },
        { book: "Song of Solomon", chapters: 8 },
        { book: "Isaiah", chapters: 66 },
        { book: "Jeremiah", chapters: 52 },
        { book: "Lamentations", chapters: 5 },
        { book: "Ezekiel", chapters: 48 },
        { book: "Daniel", chapters: 12 },
        { book: "Hosea", chapters: 14 },
        { book: "Joel", chapters: 3 },
        { book: "Amos", chapters: 9 },
        { book: "Obadiah", chapters: 1 },
        { book: "Jonah", chapters: 4 },
        { book: "Micah", chapters: 7 },
        { book: "Nahum", chapters: 3 },
        { book: "Habakkuk", chapters: 3 },
        { book: "Zephaniah", chapters: 3 },
        { book: "Haggai", chapters: 2 },
        { book: "Zechariah", chapters: 14 },
        { book: "Malachi", chapters: 4 },
        { book: "Matthew", chapters: 28 },
        { book: "Mark", chapters: 16 },
        { book: "Luke", chapters: 24 },
        { book: "John", chapters: 21 },
        { book: "Acts", chapters: 28 },
        { book: "Romans", chapters: 16 },
        { book: "1 Corinthians", chapters: 16 },
        { book: "2 Corinthians", chapters: 13 },
        { book: "Galatians", chapters: 6 },
        { book: "Ephesians", chapters: 6 },
        { book: "Philippians", chapters: 4 },
        { book: "Colossians", chapters: 4 },
        { book: "1 Thessalonians", chapters: 5 },
        { book: "2 Thessalonians", chapters: 3 },
        { book: "1 Timothy", chapters: 6 },
        { book: "2 Timothy", chapters: 4 },
        { book: "Titus", chapters: 3 },
        { book: "Philemon", chapters: 1 },
        { book: "Hebrews", chapters: 13 },
        { book: "James", chapters: 5 },
        { book: "1 Peter", chapters: 5 },
        { book: "2 Peter", chapters: 3 },
        { book: "1 John", chapters: 5 },
        { book: "2 John", chapters: 1 },
        { book: "3 John", chapters: 1 },
        { book: "Jude", chapters: 1 },
        { book: "Revelation", chapters: 22 }
      ];


    const [book, setBook] = useState('');
    const handleBookChange = (event) => {
        const v = event.target.value;
        let n = 0;
        for (const b in bible) {
            if (bible[b]["book"] === v) {
                n = bible[b]["chapters"];
            }
        }

        let c = [];
        let i = 1;
        while (i < n + 1) {
            c.push(i);
            i += 1;
        }
        setChapters(c);
        setBook(v);
    }
    const [chapter, setChapter] = useState('');
    const handleChapterChange = async (event) => {
        const v = event.target.value;
        setChapter(v);
        const url = `https://bible-api.com/${book}${v}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            const n = data.verses.length
            let c = [];
            let i = 1;
            while (i < n + 1) {
            c.push(i);
            i += 1;
            }
            setVerses(c);
        }
    }
    const [verse, setVerse] = useState('');
    const handleVerseChange = (event) => {
        const v = event.target.value;
        setVerse(v);
    }
    const [text, setText] = useState('');
    const [chapters, setChapters] = useState([]);

    const [vers, setVerses] = useState([]);

    const fetchData = async (items) => {
        let url = '';
        if (!(items[2])) {
            url = `https://bible-api.com/${items[0]}${items[1]}?verse_numbers=true`;
        } else {
            url = `https://bible-api.com/${items[0]}${items[1]}:${items[2]}?verse_numbers=true`;
        }
        const answer = await fetch(url);
        if (answer.ok) {
            const data = await answer.json();
            setText(data.text);

        }

    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = [book, chapter, verse];
        await fetchData(data);


    }





    useEffect(() => {
        document.getElementsByClassName("ql-container")[0].style.border = 0;
        document.getElementsByClassName("ql-toolbar")[0].style.backgroundColor = "rgb(225, 190, 171)";
        document.getElementsByClassName("ql-toolbar")[0].style.border = "3px solid rgb(155, 81, 41)";
        document.getElementsByClassName("ql-toolbar")[0].style.marginBottom = "8px";
        document.getElementsByClassName("ql-toolbar")[0].style.width = "442px";
        document.getElementsByClassName("ql-editor")[0].style.paddingLeft = "3px";

    }, []);





    return(
        <div>
            <button className="login-link"><NavLink className="nav-login nl" to="login">Login</NavLink></button>
            <div>
                <form className="search" onSubmit={handleSubmit}>
                    <select onChange={handleBookChange} required type="text" value={book} placeholder="Book">
                        <option value="">Book</option>
                        {bible.map(b => {
                            return (
                                <option key={b.book} value={b.book}>
                                    {b.book}
                                </option>

                            )
                        })}
                    </select>
                    <select onChange={handleChapterChange} required type="number" value={chapter} placeholder="Chapter">
                        <option value="">Chapter</option>
                        {chapters.map(t => {
                            return (
                                <option key={t} value={t}>
                                    {t}
                                </option>
                            )
                        })}
                    </select>

                    <select onChange={handleVerseChange} type="number" value={verse} placeholder="Verse">
                        <option value="">Verse</option>
                        {vers.map(ver => {
                            return (
                                <option key={ver} value={ver}>
                                    {ver}
                                </option>
                            )
                        })}
                    </select>
                    <button type="submit">Search</button>
                </form>
            </div>
            <div className="sermon">
                <div className="message">
                    <form onChange={handleSermonChange} value={sermon}>
                        <TextEditor  />
                    </form>
                </div>
                <div className="passage">
                    <textarea readOnly className="pwindow" rows={12} value={text}></textarea>
                </div>
                <div className="notes">
                    <textarea className="ntwindow" rows={10}>Verse Notes:</textarea>
                </div>
            </div>
            <button className="note-sv"><img src="https://cdn2.iconfinder.com/data/icons/strongicon-vol-22/24/tool-26-512.png" width={20} /></button>
            <button className="save"><img src="https://cdn2.iconfinder.com/data/icons/strongicon-vol-22/24/tool-26-512.png" width={20} /></button>
            <button className="present-mode"><NavLink className="nav-link nl" to="present"><img src="https://www.svgrepo.com/show/130804/expand.svg" width={20} /></NavLink></button>
            <div className="dot b1"></div>
            <div className="dot b2"></div>
            <div className="dot b3"></div>
            <div className="dot b4"></div>

        </div>
    );
}
export default Write;
