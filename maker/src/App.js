import React from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import "./styles.css";
import Write from "./Write";
import Nav from "./Nav";
import Login from "./Login";
import Presentation from "./Presentation";


function App() {
  return (
    <BrowserRouter>
    <Nav />
      <div className="nav">
        <Routes>
          <Route path="/" element={<Write />} />
          <Route path="present" element={<Presentation />} />
          <Route path="login" element={<Login />} />
        </Routes>

      </div>

    </BrowserRouter>
  );
}

export default App;
